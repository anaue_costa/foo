﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApplication1;

namespace UnitTestProject1
{
    [TestClass]
    public class ConsoleProgramTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            string expected = "44444";
            string actual = ConsoleApplication1.Program.WriteOnConsole(expected);

            Assert.AreEqual(expected, actual, "0" , "Console log is not right");
        }
    }
}
